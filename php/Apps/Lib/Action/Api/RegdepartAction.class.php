<?php

/*
 * 感谢使用微笑开发程序 如有不足 请多多指教
 * 默认是微笑独立开发的页面和程序 请不要修改发布到商业场景
 * 联系QQ 512720913  by.weixiao
 * departName 部门名称 
 * priority 显示优先级
 * parentId 上级部门id
 * status 状态
 * created 创建时间
 * updated  更新时间
 */

class RegdepartAction extends Action {
 
    public function index() {
        header("Content-Type: text/html; charset=utf-8");
        header("Cache-Control: no-cache");
        header("Pragma: no-cache");

        //接收post或者get数据 默认已经格式化输出了
        $departName =  urldecode( $this->_param('departname'));
        $priority = $this->_param('priority');
        $parentId = $this->_param('parentid');
        $sign = $this->_param('sign');

        //判断接口参数为空定义
        if (empty($departName)) {
            echo _tojson(1, "部门名称不能为空！", null);
            return;
        }
        if (empty($priority)) {
            echo _tojson(1, "显示优先级不能为空！", null);
            return;
        }
        if (empty($parentId)) {
            echo _tojson(1, "上级部门id不能为空！", null);
            return;
        }
        if (empty($sign)) {
            echo _tojson(1, "签名数据不能为空！", null);
            return;
        }
        // 开始判断签名 按照键名排序
        $prestr = $departName . $parentId . $priority ;
        if (md5Verify($prestr, $sign, C('sign_key'))) {
            //实例化部门数据表
            $Depart = M('Depart');
            //开始判断部门是否存在 
            $return_depart = $Depart->where(array("departName" => $departName))->find();
      
            if (empty($return_depart)) {

                $arr = array(
                    "departName" => $departName,
                    "priority" => $priority,
                    "parentId" => $parentId,
                    "status" => "1",
                    "created" => time(),
                );
                $return_dep = $Depart->add($arr);
                  
                if ($return_dep !== FALSE) {

                    echo _tojson(0, "ok", $code);
                    return;
                } else {
                    echo _tojson(1, "写入数据库失败", $code);
                    return;
                }
            } else {
                echo _tojson(1, $departName . "已存在", null);
                return;
            }
        } else {
            echo _tojson("1", "数据签名校验失败！", C('check_sige_err'));
        }
    }

}
