#ifndef VIDEO_RENDERER_FOR_WINDOWS_HXX
#define VIDEO_RENDERER_FOR_WINDOWS_HXX
#pragma once

#include "Call.h"

#include <memory>
#include <vfw.h>


class VideoRendererObserver
{

public:
	virtual void OnVideoFrame() = 0;

protected:
	~VideoRendererObserver(){}
};

class VideoRendererForWindows : public VideoRenderer
{
public:

	VideoRendererForWindows(VideoRendererObserver *observer);

	~VideoRendererForWindows();

	virtual void SetSize(int width, int height) override;

	virtual void RenderFrame(const char* buffer, unsigned int length) override;

	void Lock() {
		::EnterCriticalSection(&buffer_lock_);
	}

	void Unlock() {
		::LeaveCriticalSection(&buffer_lock_);
	}
	
	const BITMAPINFO& bmi() const { return bmi_; }

	const unsigned char* image() const { return image_.get(); }


private:
	BITMAPINFO bmi_;
	int width_;
	int height_;
	std::auto_ptr<unsigned char> image_;
	CRITICAL_SECTION buffer_lock_;
	VideoRendererObserver *observer_;
};


// A little helper class to make sure we always to proper locking and
// unlocking when working with VideoRenderer buffers.
template <typename T>
class AutoLock {
public:
	explicit AutoLock(T* obj) : obj_(obj) { obj_->Lock(); }
	~AutoLock() { obj_->Unlock(); }
protected:
	T* obj_;
};

#endif
